package xyz.redissi.tub;

import android.graphics.PorterDuff;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * @author Ilias
 */
public class LinesAdapter extends RecyclerView.Adapter<LinesAdapter.ViewHolder> {
    protected Line[] mDataset;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView mNumero;
        public TextView mDest1;
        public TextView mDest2;
        public ViewHolder(View v) {
            super(v);
            mNumero = (TextView) v.findViewById(R.id.numero);
            mDest1 = (TextView) v.findViewById(R.id.dest1);
            mDest2 = (TextView) v.findViewById(R.id.dest2);

            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            Log.d("Line", String.valueOf(position));
            if(position >= 0)
                Snackbar.make(v, "Ligne n°" + mDataset[position].getId(), Snackbar.LENGTH_LONG)
                    .show();
        }
    }

    public LinesAdapter(Line[] myDataset) {
        mDataset = myDataset;
    }

    @Override
    public LinesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.line_layout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.mNumero.setText(String.valueOf(mDataset[position].getId()));
        Log.d("Line color", Integer.toHexString(mDataset[position].getColor()));
        if(mDataset[position].getColor() != -1)
            holder.mNumero.getBackground().setColorFilter(mDataset[position].getColor(),
                PorterDuff.Mode.SRC);
        if(mDataset[position].getDest() != null){
            holder.mDest1.setText(mDataset[position].getDest()[0]);
            holder.mDest2.setText(mDataset[position].getDest()[1]);
        }
    }

    @Override
    public int getItemCount() {
        return mDataset.length;
    }
}
