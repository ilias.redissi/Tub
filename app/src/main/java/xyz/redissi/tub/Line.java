package xyz.redissi.tub;

/**
 * @author Ilias
 */
public class Line {
    private int id;
    private int color;
    private String[] dest;

    public Line() {
        id = -1;
        color = -1;
        dest = null;
    }

    public Line(int id) {
        setId(id);
        setColor(-1);
        setDest(null);
    }

    public Line(int id, int color) {
        setId(id);
        setColor(color);
    }

    public Line(int id, String[] dest) {
        setId(id);
        setColor(-1);
        setDest(dest);
    }

    public Line(int id, int color, String[] dest) {
        setId(id);
        setColor(color);
        setDest(dest);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public String[] getDest() {
        return dest;
    }

    public void setDest(String[] dest) {
        if(dest != null && dest.length == 2)
            this.dest = dest;
        else
            this.dest = null;
    }
}
